package cn.codetector.minecraftplugin.gamemode4.xpStorage;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerMovementListener implements Listener {
    xpStorageMain plugin;
    public PlayerMovementListener(xpStorageMain p){
        this.plugin = p;
    }
    @EventHandler
    public void PlayerMoveHandler(PlayerMoveEvent event){
        if(event.getPlayer().getLocation().add(0,0,0).getBlock().getType() == Material.ENDER_CHEST) {
            plugin.cycle.minusPlayer(event.getPlayer());
        }else if(event.getPlayer().getLocation().add(0, 2, 0).getBlock().getType() == Material.ENDER_CHEST){
            plugin.cycle.addPlayer(event.getPlayer());
        }
    }

}
